import sqlite3
from sqlite3 import Error


def create_connection(db_file):
    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except Error as e:
        print(e)
    return conn


def select_all_tasks(conn):
    cur = conn.cursor()
    cur.execute("SELECT * FROM mycollection_base_album")
    rows = cur.fetchall()
    rows = rows
    print(rows)


def select_task_by_priority(conn, priority):
    cur = conn.cursor()
    cur.execute("SELECT * FROM mycollection_base_album WHERE tytul_albumu=?", (priority,))
    rows = cur.fetchall()
    for row in rows:
        print(row)


def main():
    database = r"C:\Users\NiedźwiedźStaś\PycharmProjects\final_project\db.sqlite3"
    conn = create_connection(database)
    with conn:
        return select_all_tasks(conn)

# def wyswietl():
#     if __name__ == '__main__':
#         html = {'obj': " %s." % main()}
#         return render(request, 'mycollection_base/baza_danych.html', html)

