from django.db import models

# okladka = models.ImageField()


class Album(models.Model):
    nazwa_zespolu = models.CharField(max_length=50)
    tytul_albumu = models.CharField(max_length=50)
    gatunek = models.CharField(max_length=50)
    rok_wydania = models.CharField(max_length=50)
    opis = models.TextField()
    okladka = models.CharField(max_length=500)
    wlasciciel = models.IntegerField()
    # def __str__(self):
    #     return self.nazwa_zespolu
