from django.urls import path
from . import views

urlpatterns = [
    path('baza_danych/', views.baza_danych, name='baza_danych'),
    path('register/', views.register, name='register'),
    path('dodaj_muzyka/', views.dodaj_muzyka, name='dodaj_muzyka'),
    path('edytuj/', views.edytuj, name='edytuj'),
    path('usun/', views.usun, name='usun'),
    path('find_api', views.find_api, name='find_api'),
    path('dodaj_z_api', views.dodaj_z_api, name='dodaj_z_api'),
    path('alert', views.alert, name='alert'),
    path('login_alert', views.login_alert, name='login_alert'),
]
