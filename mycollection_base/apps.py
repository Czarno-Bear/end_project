from django.apps import AppConfig


class MycollectionBaseConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'mycollection_base'
