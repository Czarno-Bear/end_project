'''

Plik zawiera klasy zapisu uzytkownika ora danych do bazy

'''

from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from .models import Album


class UserRegistrationForm(UserCreationForm):
    username = forms.CharField(max_length=101, label="Użytkownik")
    first_name = forms.CharField(max_length=101, label="Imię")
    last_name = forms.CharField(max_length=101, label="Nazwisko")

    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email', 'password1',
                  'password2']


class DodajMuzyka(forms.ModelForm):

    class Meta:
        model = Album
        fields = ['nazwa_zespolu', 'tytul_albumu', 'gatunek', 'rok_wydania',
                  'opis', 'okladka', 'wlasciciel']
