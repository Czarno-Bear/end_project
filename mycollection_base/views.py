''' Opis funkcji:

def login - logowanie klienta do strony; login.html - formularz do logowania

def register - rejestrowanie klienta do serwisu; register.html - formularz do
    zapisania się

def login_alert - zwraca odpowiedni komentarz gdy użytkownik jest zarejestrowany

def baza_danych - główna strona serwisu (baza_danych.html), zwraca wszystkie
    obiekty użytkownika apisane w bazie na podtawie 'id'

def dodaj_muzyka - zwraca formularz do umieszczenia danych w bazie
    (dodaj_muzyka.html), oraz zapisuje je do bazy

def edytuj - pozwala na edycję wybranych danych; edytuj.html - formularz do
    edycji danych

def usun - pozwala usunac wybrane dane. usun.html - pola typu 'radio' aby
    potwierdzić ostateczną decyzje

def find_api - wyszukuje dane z API (find_api.html) i zwraca je do pliku
    dodaj_z_api.html

def dodaj_z_api - zwraca formularz z pobranymi danymi (dodaj_z_api.html)
    z możliwością edycji oraz ostatecznego zapisu danych

def alert - zwraca odpowiednie alerty po wykonaniu akcji (alert.html) '''

from json import JSONDecodeError
from django.shortcuts import render, redirect
# from mycollection.settings import BASE_DIR
from .forms import DodajMuzyka, UserRegistrationForm
from .models import Album
# import sqlite3
# import os
import requests


def login_alert(request):
    info = 'Jesteś zarejestrowany!'
    return render(request, 'mycollection_base/login_alert.html',
                  {'info': info})


def alert(request):
    return render(request, 'mycollection_base/alert.html',
                  {'user_id': request.user.id})


def login(request):
    return render(request, 'mycollection_base/login.html', {})


def baza_danych(request):

    alb = Album.objects.filter(wlasciciel=request.user.id).all()
    return render(request, 'mycollection_base/baza_danych.html',
                  {'alb': alb, 'user_id': request.user.id})


def dodaj_z_api(request):
    if request.method == 'POST':
        form = request.POST

        return render(request, 'mycollection_base/dodaj_z_api.html',
                      {'form': form})


def register(request):

    if request.method == 'POST':
        form = UserRegistrationForm(request.POST)

        if form.is_valid():
            try:
                form.save()

                return redirect('login_alert')
            except:
                pass
        else:
            context = {'form': form}
            return render(request, 'mycollection_base/register.html', context)

    else:
        form = UserRegistrationForm()
        context = {'form': form}
        return render(request, 'mycollection_base/register.html', context)


def dodaj_muzyka(request):
    if request.method == 'POST':
        form = DodajMuzyka(request.POST)

        if form.is_valid():
            form.wlasciciel = request.user.id
            form.save()
            info = 'Album dodany.'
            return render(request, 'mycollection_base/alert.html',
                          {'info': info, 'user_id': request.user.id})
        else:
            context = {'form': form, 'user_id': request.user.id,
                       'wlasciciel': request.POST.get('wlasciciel')}
            return render(request, 'mycollection_base/dodaj_muzyka.html',
                          context)

    else:
        form = DodajMuzyka()
        return render(request, 'mycollection_base/dodaj_muzyka.html',
                      {'form': form})


def edytuj(request):
    if request.method == 'GET':

        # con = sqlite3.connect(os.path.join(BASE_DIR, 'db.sqlite3'))
        # c = con.cursor()
        #
        # sql = ''' UPDATE mycollection_base_album
        #                           SET nazwa_zespolu = ? ,
        #                               tytul_albumu = ? ,
        #                               gatunek = ? ,
        #                               rok_wydania = ? ,
        #                               opis = ? ,
        #                               okladka = ?
        #                           WHERE id = ? '''
        #
        # c.execute(sql, (request.GET.get('nazwa_zespolu'),
        #                 request.GET.get('tytul_albumu'),
        #                 request.GET.get('gatunek'),
        #                 request.GET.get('rok_wydania'),
        #                 request.GET.get('opis'),
        #                 request.GET.get('okladka'),
        #                 request.GET.get('id')))
        # con.commit()
        # con.close()

        aid = request.GET.get('id')
        album = Album.objects.filter(id=aid).first()

        album.nazwa_zespolu = request.GET.get('nazwa_zespolu')
        album.tytul_albumu = request.GET.get('tytul_albumu')
        album.gatunek = request.GET.get('gatunek')
        album.rok_wydania = request.GET.get('rok_wydania')
        album.opis = request.GET.get('opis')
        album.okladka = request.GET.get('okladka')

        album.save()

        info = 'Zapisano zmiany!'
        return render(request, 'mycollection_base/alert.html',
                      {'info': info})

    if request.method == 'POST':

        form = request.POST

        return render(request, 'mycollection_base/edytuj.html', {'form': form})

    else:
        return render(request, 'mycollection_base/baza_danych.html', {})


def usun(request):
    if request.method == 'POST':
        form = request.POST
        return render(request, 'mycollection_base/usun.html', {'form': form})
    elif request.method == 'GET':
        if request.GET.get('decision') == 'tak':
            # con = sqlite3.connect(os.path.join(BASE_DIR, 'db.sqlite3'))
            # c = con.cursor()
            # sql = 'DELETE FROM mycollection_base_album WHERE id = ?'
            # c.execute(sql, (request.GET.get('id'),))
            # con.commit()
            # con.close()
            aid = request.GET.get('id')
            Album.objects.filter(id=aid).delete()

            info = 'Album usunięty!'
            return render(request, 'mycollection_base/alert.html',
                          {'info': info})
        elif request.GET.get('decision') == 'nie':
            info = 'Cofnięto decyzję usunięcia!'
            return render(request, 'mycollection_base/alert.html',
                          {'info': info})
        else:
            info = 'Nieodpowiednia akcja!'
            return render(request, 'mycollection_base/alert.html',
                          {'info': info})


def find_api(request):
    find = ''
    url = ''
    host = ''
    typ = ''
    if request.method == 'GET':
        if request.GET.get('api') == 'wykonawca':
            find = "s"
            host = "theaudiodb.p.rapidapi.com"
            typ = 'm'
            url = "https://theaudiodb.p.rapidapi.com/searchalbum.php"
        elif request.GET.get('api') == 'album':
            find = "a"
            host = "theaudiodb.p.rapidapi.com"
            typ = 'm'
            url = "https://theaudiodb.p.rapidapi.com/searchalbum.php"
        elif request.GET.get('api') == 'film':
            find = 'q'
            host = "imdb8.p.rapidapi.com"
            typ = 'f'
            url = "https://imdb8.p.rapidapi.com/auto-complete"

        querystring = {find: request.GET.get('szukaj')}

        headers = {
            'x-rapidapi-host': host,
            'x-rapidapi-key': request.GET.get('klucz_api')
        }

        try:

            response = requests.request("GET", url, headers=headers,
                                        params=querystring)

            api_dict = response.json()

            del request.GET

            return render(request, 'mycollection_base/find_api.html',
                          {'api_dict': api_dict,
                           'user_id': request.GET.get('wlasciciel'),
                           'typ': typ})
        except JSONDecodeError:
            info = 'Nastąpił błąd, proszę spróbować później.'
            return render(request, 'mycollection_base/alert.html',
                          {'info': info})
    else:
        pass
